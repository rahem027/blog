#!/bin/sh

rm -r public/
echo 'public/ deleted'

mkdir -p public/assets
cp -r assets/ styles/ favicon.ico favicon.png public/
echo 'appropriate files copied'
